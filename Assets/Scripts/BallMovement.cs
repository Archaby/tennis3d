﻿using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public float speedBall = 200f;
    Rigidbody rb;
    private bool isSpaceButtonDown = false;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if ( Input.GetKey ( KeyCode.Space ) && !isSpaceButtonDown ) 
        {
            isSpaceButtonDown = true;
            rb.isKinematic = false;
            transform.parent = null;
            rb.AddForce ( new Vector3 (speedBall, speedBall, 0) );
        }

    }
}
