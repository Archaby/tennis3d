﻿using UnityEngine;

public class BallCollision : MonoBehaviour
{
    void OnCollisionEnter ( Collision other )
    {
        if ( other.gameObject.tag == "BottomWall" )
        {
            GameManager.instance.PlayLooseSound();
            GameManager.instance.UpScoreToRival();
            Destroy ( gameObject );
        }

        else if ( other.gameObject.tag == "TopWall" )
        {
            GameManager.instance.PlayWinSound();
            GameManager.instance.UpScoreToPlayer();
            Destroy ( gameObject );
        }

        else if ( other.gameObject.tag == "BounceSurface" ) 
        {
            GameManager.instance.PlayBounceSound();
        }

    }

}
