﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRival : MonoBehaviour
{
    GameObject ball;
    float speed;
    int dx;

    string difficult;

    // Start is called before the first frame update
    void Start()
    {
        difficult = SetDifficultForGame.instance.CurDifficult;
        if ( difficult == "Easy" ) speed = 6f;
        else if ( difficult == "Medium" ) speed = 6.5f;
        else if ( difficult == "Hard" ) speed = 7f;
        refToBall();  
    }

    // Update is called once per frame
    void Update()
    {
        if ( ball != null ) 
        { 
            float ballPosY = ball.transform.position.y;
            if ( ballPosY > 0 ) 
            {
                float ballPosX = ball.transform.position.x;
                
                if ( ballPosX < transform.position.x ) dx = -1;
                else if ( ballPosX > transform.position.x ) dx = 1;

                float posXRival = transform.position.x + (0.5f * speed * Time.deltaTime * dx);
                transform.position = new Vector3( Mathf.Clamp ( posXRival, -2.38f, 2.38f ), 4.35f, 0 );
            }
        }
        else refToBall(); 
    }

    void refToBall()
    {
        ball = GameObject.FindWithTag("Ball");    
    }
}
