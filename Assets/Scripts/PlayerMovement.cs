﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f;
    //Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        //rb = GetComponent <Rigidbody> ();
    }

    void Update()
    {
        float posX = transform.position.x + ( Input.GetAxis ( "Horizontal" ) * speed * Time.deltaTime );
        transform.position = new Vector3 ( Mathf.Clamp ( posX, -2.38f, 2.38f ), -4.35f, 0 );
    }
}
