﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SetDifficultForGame : MonoBehaviour
{
    public static SetDifficultForGame instance = null;

    public string CurDifficult;

    void Awake() 
    {
        if ( instance == null )
        {
            instance = this;
        }
        else if ( instance != this )
        {
            Destroy(gameObject);
        }
    }

    public void SetEasyGame() 
    {
        CurDifficult = "Easy";
        ReloadLevel();
    } 

    public void SetMediumGame() 
    {
        CurDifficult = "Medium";
        ReloadLevel();
    }

    public void SetHardGame() 
    {
        CurDifficult = "Hard";
        ReloadLevel();
    }

    void ReloadLevel() 
    {
        SceneManager.LoadScene ( "Game" );
        Time.timeScale = 1;
    }

}
