﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject Player;
    public GameObject Ball;
    private GameObject ball;
    public GameObject MainMenu;
    public Text ScorePlayerText;
    public Text ScoreRivalText;
    private int RivalScore = 0;
    private int PlayerScore = 0;
    private bool isMainMenuSee = false;
    AudioSource audioSource;
    public AudioClip Bounce;
    public AudioClip Win;
    public AudioClip Loose;

    void Awake()
    {
        if ( instance == null )
        {
            instance = this;
        }

        else if ( instance != this )
        {
            Destroy(gameObject);
        }
        Setup();
    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    void Update()
    {
        MainMenuActive();    
    }
        
    public void UpScoreToRival()
    {
       RivalScore ++; 
       ScoreRivalText.text = RivalScore.ToString();
       Setup();  
    }

    public void UpScoreToPlayer()
    {
       PlayerScore ++; 
       ScorePlayerText.text = PlayerScore.ToString(); 
       Setup(); 
    }

    public void ResetLevel()
    {

    }

    void Setup() 
    {
        ball = Instantiate ( Ball, new Vector3 (Player.transform.position.x, -4.1f, 0), Quaternion.identity ) as GameObject;
        ball.transform.parent = Player.transform;
    }

    void PauseGame ()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame ()
    {
        Time.timeScale = 1;
    }

    void MainMenuActive()
    {
        if ( Input.GetKeyUp ( KeyCode.Escape ) )
        {
            isMainMenuSee = !isMainMenuSee;
            if (isMainMenuSee) PauseGame(); 
            else ResumeGame();
            MainMenu.SetActive(isMainMenuSee);
        }
    }

    public void PlayBounceSound() 
    {
        audioSource.PlayOneShot(Bounce);
    }

    public void PlayWinSound() 
    {
        audioSource.PlayOneShot(Win);
    }

    public void PlayLooseSound() 
    {
        audioSource.PlayOneShot(Loose);
    }

}
